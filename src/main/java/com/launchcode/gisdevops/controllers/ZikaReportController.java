package com.launchcode.gisdevops.controllers;

import com.launchcode.gisdevops.data.ZikaReportRepository;
import com.launchcode.gisdevops.features.Feature;
import com.launchcode.gisdevops.features.FeatureCollection;
import com.launchcode.gisdevops.models.ZikaReport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(value = "/zika_report")
public class ZikaReportController {


    @Autowired
    private ZikaReportRepository zikaReportRepository;

    //@RequestMapping(value = "/")
    @RequestMapping(value = {"/", ""})
    @ResponseBody
    public FeatureCollection getZikaReports(@RequestParam(name="id") Optional<Long> id)  {
        List<ZikaReport> zikaReports;
        if(id.isPresent() && !id.toString().isEmpty()) {
            zikaReports = zikaReportRepository.findById(id.get());
        } else {
            zikaReports = zikaReportRepository.findAll();
        }

        if(zikaReports.isEmpty()) { return new FeatureCollection(); }

        FeatureCollection featureCollection = new FeatureCollection();
        for (ZikaReport zikaReport : zikaReports) {
            HashMap<String, Object> properties = new HashMap<>();
            properties.put("id", zikaReport.getId());
            properties.put("location", zikaReport.getLocation());
            properties.put("locationType", zikaReport.getLocationType());
            properties.put("dataField", zikaReport.getDataField());
            properties.put("dataFieldCode", zikaReport.getDataFieldCode());
            properties.put("timePeriod", zikaReport.getTimePeriod());
            properties.put("timePeriodType", zikaReport.getTimePeriodType());
            properties.put("value", zikaReport.getValue());
            properties.put("unit", zikaReport.getUnit());
            properties.put("reportDate", zikaReport.getReportDate());

            featureCollection.addFeature(new Feature(zikaReport.getLocationGeometry(), properties));
        }
        return featureCollection;
    }
}
