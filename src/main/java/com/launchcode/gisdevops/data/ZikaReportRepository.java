package com.launchcode.gisdevops.data;

import com.launchcode.gisdevops.models.ZikaReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public interface ZikaReportRepository extends JpaRepository<ZikaReport, Long>{

    public List<ZikaReport> findByLocation(String location);
    public List<ZikaReport> findById(Long id);
}
