

$(document).ready(function () {

    const osmLayer = new ol.layer.Tile({
        source: new ol.source.OSM(),
        visible: true
    });

    const map = new ol.Map({
        target: 'mapPlaceholder',
        layers: [
            osmLayer
        ],
        view: new ol.View({
            center: ol.proj.fromLonLat([260.55, 23]),
            zoom: 4
        })
    });

    //Define style, source, and layer to show Airport features
    const pointStyle = new ol.style.Style({
        image: new ol.style.Circle({
            radius: 5,
            fill: null,
            stroke: new ol.style.Stroke({color: 'red', width: 1})
        })
    });
    const zikaReportSource = new ol.source.Vector({
        format: new ol.format.GeoJSON(),
        url: 'http://localhost:8080/zika_report/'
    });
    const zikaReportLayer = new ol.layer.Vector({
        source: zikaReportSource,
        title: "zikaReports",
//        style: function(feature) {
//
//            return pointStyle;
//        }
        style: function(feature) {

            let numCases = feature.get('value');
            let circleColor = 'red';
            let sizeStep = 95000 / 50;
            let circleRadius = 5 + numCases / sizeStep;
            circleRadius =  (circleRadius < 5) ? 5 : circleRadius;
            circleRadius =  (circleRadius > 55) ? 55 : circleRadius;
            let strokeWidth = 3;

            if (numCases < 1) {
                circleRadius = 1;
                circleColor = [40,255,40,0.3]; //RGBA: light green
                strokeWidth = 5
            }
            else if (numCases < 100) {
                circleColor = [255,120,120]; //RGB: light red
            }
            else if (numCases < 1000) {
                circleColor = [255,90,90]; //RGB: light red
            }
            else if (numCases < 10000) {
                circleColor = [255,50,50]; //RGB: light red
            }

            const ps = new ol.style.Style({
                image: new ol.style.Circle({
                    radius: circleRadius,
                    fill: null,
                    stroke: new ol.style.Stroke({color: circleColor, width: strokeWidth})
                })
            });
            return ps;
        }
    });
    map.addLayer(zikaReportLayer);

    //let routesLayers;
    //let routeName = "Test 'global' variable route name";

    //Define style, source, and layer to show Route features
    const lineStyle = new ol.style.Style({
        stroke: new ol.style.Stroke({color: 'green', width: 1})
    });
    const lineStyleFunction = function(feature) {
        return lineStyle;
    }

    map.on('click', function(event) {
        $('#zika_reports').empty();
        let reports = [];
        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
            let date = new Date(feature.get('reportDate')); // comes out of feature.get as a string representing milliseconds since epic or something similar
            date = date.toDateString();

            if (reports.length == 0) {
                reports[0] = {
                    location:     feature.get('location'),
                    locationType: feature.get('locationType'),
                    reportEntry: [
                        {
                            reportDate:     date,
                            dataField:      feature.get('dataField'),
                            dataFieldCode:  feature.get('dataFieldCode'),
                            timePeriod:     feature.get('timePeriod'),
                            timePeriodType: feature.get('timePeriodType'),
                            value:          feature.get('value'),
                            unit:           feature.get('unit')
                        }
                    ]
                };
            }
            else {
                let similarIndex = -1;
                for (let i=0; i<reports.length; i++) {
                    if (reports[i].location == feature.get('location') && reports[i].locationType == feature.get('locationType')) {
                        similarIndex = i;
                    }
                }

                if (similarIndex > -1) {
                    let len = reports[similarIndex].reportEntry.length;
                    reports[similarIndex].reportEntry[len] = {
                        reportDate:     date,
                        dataField:      feature.get('dataField'),
                        dataFieldCode:  feature.get('dataFieldCode'),
                        timePeriod:     feature.get('timePeriod'),
                        timePeriodType: feature.get('timePeriodType'),
                        value:          feature.get('value'),
                        unit:           feature.get('unit')
                    };
                }
                else {
                    let len = reports.length;
                    reports[len] = {
                        location:     feature.get('location'),
                        locationType: feature.get('locationType'),
                        reportEntry: [
                            {
                                reportDate:     date,
                                dataField:      feature.get('dataField'),
                                dataFieldCode:  feature.get('dataFieldCode'),
                                timePeriod:     feature.get('timePeriod'),
                                timePeriodType: feature.get('timePeriodType'),
                                value:          feature.get('value'),
                                unit:           feature.get('unit')
                            }
                        ]
                    };
                }
            }

        });

        let outStr = "";
        for (let i=0; i<reports.length; i++) {
            outStr += `<div>
                       <h3>Location: ${reports[i].location}</h3>
                       <h4>Location Type: ${reports[i].locationType}</h4>
                       `;
            for (let j=0; j<reports[i].reportEntry.length; j++) {
                outStr += `
                    <hr />
                    <table>
                        <tr><td>Report Date:</td><td>${reports[i].reportEntry[j].reportDate}</td></tr>
                        <tr><td>Data Field:</td><td>${reports[i].reportEntry[j].dataField}</td></tr>
                        <tr><td>Data Field Code:</td><td>${reports[i].reportEntry[j].dataFieldCode}</td></tr>
                        <tr><td>Time Period:</td><td>${reports[i].reportEntry[j].timePeriod}</td></tr>
                        <tr><td>Time Period Type:</td><td>${reports[i].reportEntry[j].timePeriodType}</td></tr>
                        <tr><td>Number of Cases:</td><td>${reports[i].reportEntry[j].value}</td></tr>
                        <tr><td>Unit:</td><td>${reports[i].reportEntry[j].unit}</td></tr>
                    </table>
                `;
            }
            outStr += `</div>`;
        }
        $('#zika_reports').append(outStr);

    });

    /*
    map.on('click', function(event) {
        //$('#airports').empty();

        let layersToRemove = [];
        map.getLayers().forEach(function (layer) {
            if (layer.get('title') != undefined && layer.get('title') === 'route') {
                layersToRemove.push(layer);
            }
        });
        for(var i = 0; i < layersToRemove.length; i++) {
            map.removeLayer(layersToRemove[i]);
        }



        map.forEachFeatureAtPixel(event.pixel, function(feature,layer) {
               //console.log("Route name = '" + routeName + "'");
               //console.log("Layer title = '" + layer.get('title') + "'");
               //console.log("Feature name = '" + feature.get('name') + "'");
               //console.log("Airport ID = '" + feature.get('airportId') + "'");
               if (layer.get('title') === "airports") {
                    const airportId = feature.get('airportId');

                    let routeSource = new ol.source.Vector({
                        format: new ol.format.GeoJSON(),
                        url: 'http://localhost:8080/route/?srcId=' + airportId
                    });
                    let routeLayer = new ol.layer.Vector({
                        source: routeSource,
                        title: "route"
                        //style: function(feature) {
                        //    return pointStyle;
                        //}
                    });
                    map.addLayer(routeLayer);


               }
        });

    });
    */


});