package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ZikaReportRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.ZikaReport;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ZikaReportControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ZikaReportRepository zikaReportRepository;

    @Before
    public void setup(){
        zikaReportRepository.deleteAll();
    }

    @After
    public void tearDown(){
        zikaReportRepository.deleteAll();
    }

    @Test
    public void zikaReportPathWorks() throws Exception {
        this.mockMvc.perform(get("/zika_report/")).andExpect(status().isOk());
        this.mockMvc.perform(get("/zika_report")).andExpect(status().isOk());
    }

    @Test
    public void zikaReportsReturnsOneElement() throws Exception {
        Date date = new Date();
        ZikaReport zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location", "String locationType", "String dataField",
                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
                );

        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features"), hasSize(1)));
    }

    @Test
    public void noZikaReportsMeansEmptyResults() throws Exception {
        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.type"), containsString("FeatureCollection")))
                .andExpect(jsonPath(("$.features"), hasSize(0)));
    }

    @Test
    public void allAttributesAreOnTheGeoJSON() throws Exception {
        Date date = new Date();
        ZikaReport zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location", "String locationType", "String dataField",
                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
        );
        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath(("$.features[0].properties.locationType"), containsString("String locationType")))
                .andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(51.1328010559082, .00001)));
                //.andExpect(jsonPath("$.features[0].geometry.coordinates[1]", closeTo(13.767200469970703, .00001))); //seems to only work on latitude? this longitude doesn't work

    }

    @Test
    public void multipleZikaReportssGivesMultipleFeatures() throws Exception {
        Date date = new Date();
        ZikaReport zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location", "String locationType", "String dataField",
                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
        );
        zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location2", "String locationType2", "String dataField2",
                        "String dataFieldCode2", "String timePeriod2", "String timePeriodType2",
                        12, "String unit2", WktHelper.wktToGeometry("POINT(14.767200469970703 52.1328010559082)"))
        );
        this.mockMvc.perform(get("/zika_report"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(2)));
    }

    @Test
    public void searchByZikaReportLocationAndId() throws Exception {
        Date date = new Date();
        ZikaReport zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location", "String locationType", "String dataField",
                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
        );
        //System.out.println("zikaReport ID = " + zikaReport.getId() );
        zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location2", "String locationType2", "String dataField2",
                        "String dataFieldCode2", "String timePeriod2", "String timePeriodType2",
                        14, "String unit2", WktHelper.wktToGeometry("POINT(14.767200469970703 52.1328010559082)"))
        );
        //System.out.println("zikaReport ID = " + zikaReport.getId() );
        this.mockMvc.perform(get("/zika_report?id=" + zikaReport.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.features", hasSize(1)))
                .andExpect(jsonPath("$.features[0].properties.id", equalTo(zikaReport.getId().intValue())));
    }

}
