package com.launchcode.gisdevops;

import com.launchcode.gisdevops.data.ZikaReportRepository;
import com.launchcode.gisdevops.features.WktHelper;
import com.launchcode.gisdevops.models.ZikaReport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@IntegrationTestConfig
public class ZikaReportRepositoryTest {


    @Autowired
    private ZikaReportRepository zikaReportRepository;

    @Autowired
    private EntityManager entityManager;

    @Before
    public void setup(){
        zikaReportRepository.deleteAll();
    }

    @Test
    public void findBySrcReturnsMatchingSrc() {
        Date date = new Date();
        ZikaReport zikaReport = zikaReportRepository.save(
                new ZikaReport(
                        date, "String location", "String locationType", "String dataField",
                        "String dataFieldCode", "String timePeriod", "String timePeriodType",
                        12, "String unit", WktHelper.wktToGeometry("POINT(13.767200469970703 51.1328010559082)"))
        );

        entityManager.persist(zikaReport);
        entityManager.flush();

        List<ZikaReport> foundZikaReport = zikaReportRepository.findByLocation(zikaReport.getLocation());

        assertEquals(1, foundZikaReport.size());

    }

}
